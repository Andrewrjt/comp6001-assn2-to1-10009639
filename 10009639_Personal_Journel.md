#Wensday 30th of August#

On this day we recived out assignments to be handed out. Due to issues on my side i was not there to recive it at the tutorial. 
I however managed to make it into the campus late in the afternoon where some of the other team members and they were able to fill me in on the project. 
Discussed a few very early concepts, also to a degree expectations of what everyone wanted from this were losely setup. Was told as well of the visit to the windermere campus to visit the area of our project.

#Thursday 31st of August#

On this day we went to the J block of the windmere campus as a team with Jacob. 
While on our way there Jacob took us by the libary or the campus aswell as one of the new blocks that had recently been finished, the purpose of these visits was to get inspiration for what type of layout he wanted the classes to be. 
A very open ended expiernce with a emphasis on new age teaching practices, concepts such as workspace type seating rather than row seats. 
For me i was told what rooms were mine and I had the chance to view them, which was the bathroom area and room j124. 
Thought the I have a smaller area than others i will have to make a great deal of custom assest soley for my area such as toliets urinals and mirrors. What is going in J124 is still not decided on yet.

#Monday 4th of September#

On this day was the first proper meeting to set out what we wanted to do with our areas and assigned what assests were going to be made by who. 
We started with assests generally because mine were exclusive to my area I was put in charge of making the, (toliets Urinals etc) i was also put in charge of making the laptop assest to be used in other areas. 
I tried to put the discussion towards what rooms were what as this would help with wireframe/floorplan devolpment without it we could not start it. 
For me I did not have such a hard time as my room stays the same with the toliets but the area of j124 had not been decided on and i wanted to know what as a team we were going to agree what it was going to become. 
It was decided on to be a PC deconstruction lab.

#Wensday 6th of September#

On this day i sat down with Sam at the tutorial to discuss specifics of the assesment to meet the criteria of milestone 1. 
One of the first things we talked about was the wireframes and how they were to be done I wanted to make sure that if we were doing them for our individual areas that the keys for all the objects were going to be the same as it at that time had been discussed to do drawn copies rather than use software. 
When we started talked about how we wanted/needed uniformed keys it came to mind that we should use visio to make the floor and roof plans, with the visio the reasoning was that it would be kept uniformed and easy to read between the different rooms being done by different people rather than using hand drawn objects which would look drastically different based on style. 
It was at this time it was agreed aswell to keep using slack as it was what is familiar and had been used before/was already established. Other options were considered but were turned down due mainly to the convnience of preestablished communications.
Also in regards to my room j124 was changed to the staffroom. This is due to changes in other peoples rooms mainly where the server was going to be placed and we didnt want the lab at a close proximity. So i will be also making the custom assests for the staffroom (sink, fridge etc)

#Friday 8th of September#

Today I stayed at home to work on this journel and began working on the wireframes. 
Due to other commitments i was unable to go to the meeting yesterday and there it was agreed that we would use Trello as means to manage the project tasks, the reasoning for it was that is was free and that it did its function simple people are assigned or can choose a task on the project that then flags them for that task then on trello once the task is done they check off that they have completeted it. 
Also at the meeting it was decided that my room j124 was to be changed from the staff room to the VR room the reason for this was the size of j124 worked better for VR and with no windows lighting was not going to be a issue. 
A concern that I have is that with the VR custom assests needed aswell as the other assests needed for the toliets and the laptop I may not be able to do all of that in the time givin for the assignment.

#Saturday 9th of September#

Worked on the changes for the wireframe/floorplan of the j124 to meet the new criteria of it being a VR room. 
I also called Sam to clarify what assests will be needed which are VR headet, positioning poles and screens. 
I also talked about my concerns regarded the larger amount of work in relation the the rest of the team to which we worked out that we could delegate some of the VR assests to people who are only doing 3-5 assests compared to my 10.

#Monday 11th of September#

Hand in day for the first part of the assesment. I have finished my areas but Jermey has not done his part of the floor plan, i am doing parts of his so that we can get it finished. 
Also talked with the team in regards to VR assests it was agreed Mark will do parts of the VR assests which will help greatly.



